/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scuro.maven.plugins.path.iterator.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class IteratorMojoTest {

    IteratorMojo iteratorMojo = new IteratorMojo();

    public IteratorMojoTest() throws IOException {
        File deepestLevel = new File(System.getProperty("java.io.tmpdir") + File.separator + "sub" + File.separator + "below");
        deepestLevel.mkdirs();

        File file = new File(deepestLevel, "testfile.txt");
        file.createNewFile();

        iteratorMojo.pathToIterate = new File(System.getProperty("java.io.tmpdir") + File.separator + "sub");
        iteratorMojo.extensionFilter = "txt";
    }

    @Test
    public void testExecute() throws MojoExecutionException {
        iteratorMojo.arguments = new String[]{"echo", "$ABSOLUTE_PATH $RELATIVE_PATH $PARENT $FILENAME $EXTENSION $BASENAME"};
        iteratorMojo.execute();
    }
}
