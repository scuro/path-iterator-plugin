package nl.scuro.maven.plugins.path.iterator.plugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Goal which recursively iterates a folder and execute any given command on the each file.
 *
 */
@Mojo(name = "interate", defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class IteratorMojo
        extends AbstractMojo {

    enum TOKEN {
        $ABSOLUTE_PATH,
        $RELATIVE_PATH,
        $PARENT,
        $FILENAME,
        $EXTENSION,
        $BASENAME
    }

    @Parameter(property = "pathToIterate", required = true)
    File pathToIterate;
    @Parameter(property = "extensionFilter", required = true)
    String extensionFilter;
    @Parameter
    String[] arguments;

    public void execute()
            throws MojoExecutionException {
        File f = pathToIterate;
        if (!f.exists()) {
            throw new MojoExecutionException("Path " + pathToIterate.getAbsolutePath() + " does not exist");
        }

        try {
            List<Path> paths = Files.walk(pathToIterate.toPath()).filter(path -> path.toString().endsWith(extensionFilter)).collect(Collectors.toList());
            for (Path path : paths) {
                List<String> args = Stream.of(arguments).map(argument -> replaceTokens(argument, path)).collect(Collectors.toList());
                ProcessBuilder pb = new ProcessBuilder(args);
                pb.inheritIO();
                pb.start().waitFor();
            }
        } catch (IOException ex) {
            throw new MojoExecutionException("Error iterating path", ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(IteratorMojo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String replaceTokens(String arg, Path path) {
        String argWithOptionalToken = arg;
        if (containsToken(argWithOptionalToken, TOKEN.$ABSOLUTE_PATH)) {
            argWithOptionalToken = replaceToken(TOKEN.$ABSOLUTE_PATH, argWithOptionalToken, path.toAbsolutePath().toString());
        }
        if (containsToken(argWithOptionalToken, TOKEN.$RELATIVE_PATH)) {
            argWithOptionalToken = replaceToken(TOKEN.$RELATIVE_PATH, argWithOptionalToken, pathToIterate.toPath().relativize(path).toString());
        }
        if (containsToken(argWithOptionalToken, TOKEN.$PARENT)) {
            argWithOptionalToken = replaceToken(TOKEN.$PARENT, argWithOptionalToken, path.getParent().getFileName().toString());
        }
        if (containsToken(argWithOptionalToken, TOKEN.$FILENAME)) {
            argWithOptionalToken = replaceToken(TOKEN.$FILENAME, argWithOptionalToken, path.getFileName().toString());
        }
        if (containsToken(argWithOptionalToken, TOKEN.$EXTENSION)) {
            final String fileName = path.getFileName().toString();
            argWithOptionalToken = replaceToken(TOKEN.$EXTENSION, argWithOptionalToken, fileName.substring(fileName.lastIndexOf('.')));
        }
        if (containsToken(argWithOptionalToken, TOKEN.$BASENAME)) {
            final String fileName = path.getFileName().toString();
            argWithOptionalToken = replaceToken(TOKEN.$BASENAME, argWithOptionalToken, fileName.substring(0, fileName.lastIndexOf('.')));
        }

        return argWithOptionalToken;
    }

    private boolean containsToken(String argument, TOKEN token) {
        if (argument == null) {
            return false;
        }
        return argument.contains(token.toString());
    }

    private String replaceToken(TOKEN token, String arg, String value) {
        return arg.replace(token.toString(), value);
    }
}
